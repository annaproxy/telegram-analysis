from parser import load_chats, concat_my_messages
from plottools import *
from add_info import apply_regexes


def example():
    # load all chats
    chats = load_chats("data/result.json")

    # concat
    all_mine = concat_my_messages(chats)

    plot_frequency_stacked(all_mine, name="chatname", freq="W", relative=True)
    plt.show()


def sleep_example():

    chats = load_chats("data/result.json")
    all_mine = concat_my_messages(chats)

    # Only plot for 2021 example
    # per_hour_data is just a matrix if you want to play
    per_hour_data = calc_plot_sleep_schedule(
        all_mine, threshold=1, startdate=datetime(2021, 1, 1).date(), tick_every=5
    )


def language_example():
    regexes = [
        ("haha", r"[wjb]?a?ha+(ha+)+h*"),
        ("hehe", r"he(he)+h?"),
        ("hmm", r"\bhm(m)*\b"),
        ("lol", r"\bl+o+ll*\b"),
        ("xd", r"\bxx*dd*\b"),
        ("omg", r"\bo+m+f*g+\b"),
        ("wow", r"\bw+o+w+(w+o+w+)*\b"),
        ("no", r"\bno+\b"),
        ("yes", r"\bye+s+\b"),
        ("ugh", r"\bu+g+h+\b"),
        ("fuck", r"\bf+u+c+k+\b"),
        ("what", r"\bw+h*a+t+\b"),
        ("kid", r"\bk+i+d+\b"),
        ("ooh", r"\bo+h*\b"),
    ]
    # takes long
    chats = load_chats("data/result.json")

    # this will extract all your own msgs
    all_mine = concat_my_messages(chats, name="Anna")

    apply_regexes(all_mine, regexes)

    plot_regexes(all_mine, [z[0] for z in regexes], title="Variations on words")
