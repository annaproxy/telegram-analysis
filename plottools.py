import matplotlib.pyplot as plt
import seaborn as sns
from tqdm import tqdm
import pandas as pd
from datetime import datetime
import random
from add_info import add_date_info
import numpy as np
import math


def line_format_week(label):
    """
    Convert time label to the format of pandas line plot
    """
    hour = label.hour
    result = f"{hour}" if hour % 6 == 0 else ""
    if hour == 0:
        result += f"\n{label.day}-{label.month_name()[:3]}"
    return result


def line_format_year(label):
    """
    Convert time label to the format of pandas line plot
    """
    month = label.month_name()[:3]
    if month == "Jan":
        month += f"\n{label.year}"
    return month


def get_recent(df, startdate):
    return df["date"] >= startdate


def plot_frequency(
    df, freq="D", startdate=datetime(2015, 1, 1), name="from", plot_total=False
):
    """
    Plots frequency without any fancy stuff
    df: Dataframe of telegram msgs.
    freq: pandas grouper freq, see also https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#offset-aliases
    startdate: startdate. plot will begin when relevant
    name: column name of the df: what to select on. i.e. 'from' to group by sender of message
    """
    names = df[name].unique()
    grouper = pd.Grouper(key="date", origin=startdate, freq=freq)
    recent = get_recent(df, startdate)
    total = df[recent].groupby([grouper]).size()

    plt.figure(figsize=(8, 4))
    if plot_total:
        total.plot()
    else:
        for n in names:
            if pd.isna(n):
                continue
            sub = df[(df[name] == n) & recent].groupby(grouper).size()
            sub.plot(label=n)
    sub.set_xticklabels(map(line_formatter, df.index))
    plt.legend()
    plt.show()


def plot_frequency_stacked(
    df,
    freq="D",
    startdate=datetime(2015, 1, 1),
    name="from",
    relative=False,
    ticks_freq="M",
    anon=True,
    barplot=False,
    line_formatter=line_format_year,
):
    """
    Plots frequency of messages

    df: Dataframe of telegram msgs.
    freq: pandas grouper freq, see also https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#offset-aliases
    startdate: startdate. plot will begin when relevant
    name: column name of the df: what to select on. i.e. 'from' to group by sender of message
    relative: Whether to divide by the total amt of messages.
    ticks_freq: pandas grouper freq for the x-axis labels
    anon: whether to not show full (chat)names
    """
    names = df[name].unique()

    # Not enough colors
    my_own_damn_palette = (
        [
            "#e6194b",
            "#3cb44b",
            "#ffe119",
            "#4363d8",
            "#f58231",
            "#911eb4",
            "#46f0f0",
            "#f032e6",
            "#469990",
            "#000075",
        ]
        + list(sns.color_palette("mako", 8))
        + list(sns.color_palette("summer", 8))
    )
    if len(names) < 4:
        random.shuffle(my_own_damn_palette)
    color_list = list(sns.color_palette(my_own_damn_palette))

    grouper = pd.Grouper(
        key="date", origin=startdate, freq=freq, dropna=False
    )
    recent = get_recent(df, startdate)
    names = df[recent][name].unique()

    ticks_group = (
        df[recent]
        .groupby([pd.Grouper(key="date", origin=startdate, freq=ticks_freq, dropna=False)])
        .size()
        .index
    )
    total = df[recent].groupby([grouper]).size()

    figsize = (12 * [1, 2][len(total) > 130],
               max(4, min(10, 4 * (len(names) / 5))))

    if not barplot:

        last = total.copy()
        last -= last

        plt.figure(
            figsize=figsize
        )

        # Fill between for every unique person
        for color, n in zip(color_list, names):
            if pd.isna(n):
                continue
            current = df[(df[name] == n) & recent]
            if len(current) < 1:
                continue
            current = current.groupby(grouper).size()
            if relative:
                current = current.div(total, fill_value=0)
            plt.fill_between(
                total.index,
                last,
                last.add(current, fill_value=0)if type(
                    last) is not int else current,
                label=n[:2] if anon else n,
                color=color,
            )
            last = last.add(current, fill_value=0) if type(
                last) is not int else current
        plt.xticks(ticks_group, map(line_formatter, ticks_group))

    else:
        current = df[recent].groupby([grouper, name]).size().unstack()
        ax = current.plot.bar(
            stacked=True, figsize=figsize, rot=0, color=color_list)
        ax.set_xticklabels(map(line_formatter, current.index))

    plt.ylabel("Percentage of Messages sent")
    plt.legend()
    plt.show()


def plot_grouped(df, startdate=datetime(2015, 1, 1), by="from"):
    """
    Plots barplot with bar labels given a groupby column name
    df: a dataframe
    startdate: startdate. plot will begin when relevant
    by: what to group by. default is 'from' (sender of message)
    """
    relevant = df[df.date > startdate].groupby(by).size()
    ax = relevant.plot.bar(
        color=sns.color_palette("twilight_shifted", len(df[by].unique()))
    )
    for container in ax.containers:
        ax.bar_label(container)
    plt.show()


def calc_plot_sleep_schedule(
    df,
    threshold=1,
    startdate=datetime(2015, 1, 1).date(),
    enddate=datetime.now().date(),
    height=20,
    tick_every=30,
):
    """
    Plots binary "any messages sent" over time.
    Will take a while!!!! should probably store somewhere for reusability

    df: any dataframe, but you probably want the concatenated one with all your messages
    threshold: threshold to apply for binarizing
    startdate, enddate: what range to plot
    height: height of the plot
    tick_every: one in how many days to plot on the y-axis. 30 is sane for year(s) long experiments.

    """
    df = df.sort_values(by="date")
    if "half_hour" not in df.columns:
        add_date_info(df)

    unique_days = df["day"].unique()
    unique_days = [d for d in unique_days if startdate < d < enddate]
    rows = []
    for date in tqdm(unique_days):
        sub = df[df["day"] == date]
        msg_per_hour = [len(sub[sub.half_hour == hour])
                        for hour in np.arange(0, 24, 0.5)]
        rows.append(msg_per_hour)
    perhour = np.array(rows)
    perhour_bin = perhour > threshold

    dates = np.array(unique_days)
    sample = np.arange(0, len(dates), tick_every)
    hours = np.arange(0, 24, 0.5)

    plt.figure(figsize=(7, height))
    sns.heatmap(perhour_bin, cbar=False)
    plt.yticks(sample, dates[sample])
    plt.xticks(hours*2, hours)

    plt.show()

    return dates, perhour


def plot_regexes(df, regex_list, title="Variations on words"):
    """
    Plots most frequent occurences of word variations in subplots.

    df: Any telegram msgs dataframe
    regex_list: list of column names of the df where the variations reside (extracted with apply_reglex)
    title: plot title
    """
    rows = math.ceil(math.sqrt(len(regex_list)))
    columns = math.ceil(len(regex_list) / rows)
    fig, axs = plt.subplots(nrows=rows, ncols=columns, figsize=(12, 10))
    for ax, r in zip(axs.flat, regex_list):
        result = (
            df[r]
            .value_counts()[:12][::-1]
            .to_frame()[r]
            .plot.barh(color=list(sns.color_palette("mako", 2)), ax=ax)
        )

        for container in result.containers:
            result.bar_label(container)

        result.legend().set_visible(False)
    plt.suptitle(title)

    plt.tight_layout()
    plt.show()
