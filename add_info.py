import pandas as pd
import re


def add_date_info(df):
    df["hour"] = df["date"].apply(lambda x: x.hour)
    df["day"] = df["date"].apply(lambda x: x.date())
    df["weekday"] = df["date"].apply(lambda x: x.day_of_week)
    df["hour"] = df["date"].apply(lambda x: x.hour)
    df["half_hour"] = df["date"].apply(lambda x: x.hour + 0.5*(x.minute >= 30))

    return df


def find_longest_regex(h, R):
    match = [h[0] for h in re.finditer(R, h, flags=re.IGNORECASE)]
    if match:
        return match[0]
    return None


def apply_regexes(df, regex_list, redo=True):
    for name, regex in regex_list:
        if name not in df.columns or redo:
            df[name] = df.text.apply(find_longest_regex, args=[regex])
