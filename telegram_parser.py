from tqdm import tqdm
from datetime import datetime
import json
import pandas as pd


def parse_date(m: str, default=datetime(2016, 1, 1)):
    if pd.isna(m):
        return default
    return datetime.strptime(m, "%Y-%m-%dT%H:%M:%S")


def _messages_to_df(messages):
    # Pandas is fucking awful
    # So I'm going to add dummy variables to every person
    #first_m = messages[0]
    #last_m = messages[-1]

    # Assume that first 5000 messages contain all relevant senders....
    # senders = set([m["from"] for m in messages[:5000] if "from" in m])
    # begin = []
    # for s in senders:
    #     b = first_m.copy()
    #     e = last_m.copy()
    #     b["from"] = s
    #     e["from"] = s
    #     begin.append(b)
    #     messages.append(e)

    # messages = begin + messages

    df = pd.DataFrame(messages)
    for z in ["date", "edited"]:
        if z in df.columns:
            df[z] = df[z].apply(parse_date)

    # Remove empty (img) columns and stuff I don't care about
    # #TODO maybe don't if you exported media.
    for colname in [
        "file",
        "thumbnail",
        "performer",
        "title",
        "photo",
        "duration_seconds",
        "self_destruct_period_seconds",
    ]:
        if colname in df.columns:
            df.drop(colname, axis=1, inplace=True)

    return convert_types(df)


def convert_types(df):
    types = {z: "Int32" for z in df.columns if "id" in z or z in [
        "width", "height"]}
    types.update(
        {z: str for z in df.columns if z in [
            "text", "type", "from", "chatname"]}
    )
    # Set ids to int
    df = df.astype(types)
    return df


def load_chats(filename="data/result.json"):
    """
    Loads all telegram exported chat data into dict of pandas dataframes.
    """
    with open(filename, "r") as f:
        data = json.load(f)
    chats = data["chats"]["list"]
    ids = [
        (z["id"], z["name"] if "name" in z else z["type"])
        for z in chats
        if len(z["messages"]) > 1
    ]
    result = {
        name: _messages_to_df([i["messages"]
                              for i in chats if i["id"] == idx][0])
        for idx, name in tqdm(ids)
    }
    return result


def concat_my_messages(chats, name="Anna", threshold=300):
    """
    Returns dataframe of only messages sent by you.

    chats: dictionary of dataframes
    name: your name (2017)
    threshold: exclude chats that dont have at least threshold messages to avoid clutter
    """
    all_my_messages = []

    # Add another column called 'chatname'
    for c in chats:
        if "from" in chats[c].columns:
            sub = chats[c][chats[c]["from"] == name]
            if len(sub) > threshold:
                sub.loc[:, "chatname"] = c
                all_my_messages.append(sub)

    all_my_messages = pd.concat(all_my_messages)

    return all_my_messages
